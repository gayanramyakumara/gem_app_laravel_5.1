<!-- resources/views/auth/register.blade.php -->

{{--<form method="POST" action="{{ url('auth/register') }}">--}}
    {{--{!! csrf_field() !!}--}}

    {{--<div>--}}
        {{--Name--}}
        {{--<input type="text" name="name" value="{{ old('name') }}">--}}
    {{--</div>--}}

    {{--<div>--}}
        {{--Email--}}
        {{--<input type="email" name="email" value="{{ old('email') }}">--}}
    {{--</div>--}}

    {{--<div>--}}
        {{--Password--}}
        {{--<input type="password" name="password">--}}
    {{--</div>--}}

    {{--<div>--}}
        {{--Confirm Password--}}
        {{--<input type="password" name="password_confirmation">--}}
    {{--</div>--}}

    {{--<div>--}}
        {{--<button type="submit">Register</button>--}}
    {{--</div>--}}
{{--</form>--}}



<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from jesus.gallery/everest/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 22 Sep 2015 06:45:43 GMT -->
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Everest Admin Panel" />
    <meta name="keywords" content="Admin, Dashboard, Bootstrap3, Sass, transform, CSS3, HTML5, Web design, UI Design, Responsive Dashboard, Responsive Admin, Admin Theme, Best Admin UI, Bootstrap Theme, Wrapbootstrap, Bootstrap" />
    <meta name="author" content="Bootstrap Gallery" />
    <link rel="shortcut icon" href="img/favicon.ico">
    <title>Everest Admin Panel</title>



    <!-- Bootstrap CSS -->
    <link href="{{ URL::asset('tmp_assets/css/bootstrap.css') }}" rel="stylesheet" media="screen">

    <!-- Animate CSS -->
    <link href="{{ URL::asset('tmp_assets/css/animate.css') }}" rel="stylesheet" media="screen">

    <!-- Main CSS -->
    <link href="{{ URL::asset('tmp_assets/main.css') }}" rel="stylesheet" media="screen">

    <!-- Main CSS -->
    <link href="{{ URL::asset('tmp_assets/css/login.css') }}" rel="stylesheet">

    <!-- Font Awesome -->
    <link href="{{ URL::asset('tmp_assets/fonts/font-awesome.min.css') }}" rel="stylesheet">

    <!-- HTML5 shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="{{ URL::asset('tmp_assets/js/html5shiv.js') }}"></script>
    <script src="{{ URL::asset('tmp_assets/js/respond.min.js') }}"></script>
    <![endif]-->

</head>

<body>
<!-- Container Fluid starts -->
<div class="container-fluid">
    <div class="row">
        <div class="col-md-push-4 col-md-4 col-sm-push-3 col-sm-6 col-sx-12">

            <!-- Header end -->
            <div class="login-container">
                <div class="login-wrapper animated flipInY">

                    <div id="register" class="show">
                        <div class="login-header">
                            <h4>Sign Up for Everest</h4>
                        </div>

                        <div class="panel-body" style="border: none">


                            @foreach($errors->all() as $error)

                                <div class="form-group has-feedback">
                                    <div class="alert alert-danger alert-white rounded no-margin">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                                        <div class="icon"><i class="fa fa-fire"></i></div>
                                        <strong>Oh snap!</strong> {{ $error }}
                                    </div>
                                </div>
                            @endforeach

                        </div>

                        {!!Form::open(array('action' => 'Auth\AuthController@postRegister','method'=>'POST'))!!}

                            {!! csrf_field() !!}
                            <div class="form-group has-feedback">
                                {!! Form::label('name', ' User Name', array('for' => 'name','class'=>'control-label'))!!}
                                {!! Form::text('name', old('name') , array('class' => 'form-control', 'id'=>'name','placeholder'=>'Name'))!!}
                                <i class="fa fa-user form-control-feedback"></i>
                            </div>
                            <div class="form-group has-feedback">
                                {!! Form::label('email', 'Email', array('for' => 'email','class'=>'control-label'))!!}
                                {!! Form::text('email', old('email') , array('class' => 'form-control', 'id'=>'email','placeholder'=>'Email'))!!}
                                <i class="fa fa-key form-control-feedback"></i>
                            </div>

                            <div class="form-group has-feedback">
                                {!! Form::label('role', 'Role', array('for' => 'role','class'=>'control-label'))!!}
                                {!! Form::select('role', array('0' => 'User','1' => 'Admin'),  old('role'), ['class' => 'form-control','id'=>'role']) !!}
                                <i class="fa fa-key form-control-feedback"></i>
                            </div>

                            <div class="form-group has-feedback">

                                {!! Form::label('password', 'Password', array('for' => 'password','class'=>'control-label'))!!}
                                {!! Form::password('password',   array('class' => 'form-control', 'id'=>'password','placeholder'=>'*********'))!!}

                                <i class="fa fa-key form-control-feedback"></i>
                            </div>

                            <div class="form-group has-feedback">
                                {!! Form::label('password_confirmation', 'Confirm password', array('for' => 'password_confirmation','class'=>'control-label'))!!}
                                {!! Form::password('password_confirmation',   array('class' => 'form-control', 'id'=>'password_confirmation','placeholder'=>'*********'))!!}

                                <i class="fa fa-key form-control-feedback"></i>
                            </div>

                            <input type="submit" value="Sign Up" class="btn btn-danger btn-lg btn-block">

                        {!! Form::close() !!}

                        <a href="#login">Already have an account? <span class="text-danger">Sign In</span></a>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>
<!-- Container Fluid ends -->

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="{{ URL::asset('tmp_assets/js/jquery.js') }}"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="{{ URL::asset('tmp_assets/js/bootstrap.min.js') }}"></script>

<script type="text/javascript">
    (function($) {
        // constants
        var SHOW_CLASS = 'show',
                HIDE_CLASS = 'hide',
                ACTIVE_CLASS = 'active';

        $('a').on('click', function(e){
            e.preventDefault();
            var a = $(this),
                    href = a.attr('href');

            $('.active').removeClass(ACTIVE_CLASS);
            a.addClass(ACTIVE_CLASS);

            $('.show')
                    .removeClass(SHOW_CLASS)
                    .addClass(HIDE_CLASS)
                    .hide();

            $(href)
                    .removeClass(HIDE_CLASS)
                    .addClass(SHOW_CLASS)
                    .hide()
                    .fadeIn(550);
        });
    })(jQuery);
</script>
</body>

<!-- Mirrored from jesus.gallery/everest/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 22 Sep 2015 06:45:44 GMT -->
</html>
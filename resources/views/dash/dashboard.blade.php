@extends('layouts.master')

@section('title', 'Dashboard')

@section('content')



        <!-- Container fluid Starts -->
<div class="container-fluid">

    <!-- Spacer starts -->
    <div class="spacer">

        <div class="row">



            <ol class="breadcrumb">
                <li><a href="#" data-original-title="" title="">Home</a></li>
                <li><a href="#" data-original-title="" title="">Dashboard</a></li>
                <li class="active">Charts</li>
            </ol>



        </div>

        <!-- Row start -->
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="blog blog-success no-margin">
                    <div class="blog-header">
                        <h5 class="blog-title">Sales Stats</h5>
                    </div>
                    <div class="blog-body">
                        <div class="table-responsive">
                            <table class="table table-condensed table-bordered no-margin">
                                <tbody>
                                <tr>
                                    <td>
                                        <span class="chart" id="spark_1">
                                            <canvas width="14" height="14" style="display: inline-block; vertical-align: top; width: 14px; height: 14px;"></canvas></span>
                                    </td>
                                    <td>
                                        Wilson
                                    </td>
                                    <td>
                                        19275, Walnut Creek, CA
                                    </td>
                                    <td>
                                        4531
                                    </td>
                                    <td>
															<span class="badge badge-info">
																Processing
															</span>
                                    </td>

                                </tr>
                                <tr>
                                    <td>
                                        <span class="chart" id="spark_2"><canvas width="14" height="14" style="display: inline-block; vertical-align: top; width: 14px; height: 14px;"></canvas></span>
                                    </td>
                                    <td>
                                        Jason
                                    </td>
                                    <td>
                                        18932, Wilsan Gardan Broadway, SU
                                    </td>
                                    <td>
                                        9852
                                    </td>
                                    <td>
															<span class="badge badge-success">
																Sent
															</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="chart" id="spark_3"><canvas width="14" height="14" style="display: inline-block; vertical-align: top; width: 14px; height: 14px;"></canvas></span>
                                    </td>
                                    <td>
                                        Robson
                                    </td>
                                    <td>
                                        11217, Church Street, SN
                                    </td>
                                    <td>
                                        1744
                                    </td>
                                    <td>
															<span class="badge badge-danger">
																Cancelled
															</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="chart" id="spark_4"><canvas width="14" height="14" style="display: inline-block; vertical-align: top; width: 14px; height: 14px;"></canvas></span>
                                    </td>
                                    <td>
                                        Karan
                                    </td>
                                    <td>
                                        3229, North Broadway, GN
                                    </td>
                                    <td>
                                        4321
                                    </td>
                                    <td>
															<span class="badge badge-info">
																Processing
															</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="chart" id="spark_5"><canvas width="14" height="14" style="display: inline-block; vertical-align: top; width: 14px; height: 14px;"></canvas></span>
                                    </td>
                                    <td>
                                        Lasan
                                    </td>
                                    <td>
                                        2318, New Lason Road, BS
                                    </td>
                                    <td>
                                        245
                                    </td>
                                    <td>
															<span class="badge badge-success">
																Sent
															</span>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Row end -->

    </div>
    <!-- Spacer ends -->

</div>
<!-- Container fluid ends -->





@stop
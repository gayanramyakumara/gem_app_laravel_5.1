@extends('layouts.master')

@section('title', 'Dashboard')

@section('content')



        <!-- Container fluid Starts -->
<div class="container-fluid">

    <!-- Spacer starts -->
    <div class="spacer">

        <div class="row">



            <ol class="breadcrumb">
                <li><a href="#" data-original-title="" title="">Home</a></li>
                <li><a href="#" data-original-title="" title="">Dashboard</a></li>
                <li class="active">Charts</li>
            </ol>



        </div>

        <!-- Row start -->
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="blog  no-margin">
                    <div class="blog-header">
                        <h5 class="blog-title">{{ $user_details[0]->name }}  profile</h5>
                    </div>
                    <div class="blog-body">



                            <div class="row">
                                <div class="col-md-2 col-lg-2 " align="center">
                                    <img alt="User Pic" src="{{URL::to('tmp_assets/img/user1.jpg')}}" class="img-responsive">
                                    <br/>

                                    <br/>

                                </div>


                                <div class=" col-md-9 col-lg-9 ">
                                    <table class="table table-user-information">
                                        <tbody>
                                        <tr>
                                            <td>Name</td>
                                            <td>{{ $user_details[0]->name }}</td>
                                        </tr>
                                        <tr>
                                            <td>Email</td>
                                            <td><a href="mailto:info@support.com">{{ $user_details[0]->email }}</a></td>
                                        </tr>

                                        <tr>
                                            <td>Role  :</td>
                                            <td>

                                                @if($user_details[0]->role ==1)

                                                                <span class="badge danger-bg">Admin</span>


                                                @else

                                                    <span class="badge danger-bg">User</span>


                                                @endif


                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Created Date/Time</td>
                                            <td>{{ $user_details[0]->created_at }}</td>
                                        </tr>

                                        {{--<tr>--}}
                                        {{--</tr>--}}
                                        {{--<tr>--}}
                                            {{--<td>Gender</td>--}}
                                            {{--<td>Male</td>--}}
                                        {{--</tr>--}}
                                        {{--<tr>--}}
                                            {{--<td>Home Address</td>--}}
                                            {{--<td>Metro Manila,Philippines</td>--}}
                                        {{--</tr>--}}

                                        {{--<tr>--}}
                                            {{--<td>Phone Number</td>--}}
                                            {{--<td>123-4567-890(Landline)--}}
                                                {{--<br><br>--}}
                                                {{--555-4567-890(Mobile)--}}
                                            {{--</td>--}}

                                        {{--</tr>--}}

                                        </tbody>
                                    </table>

                                    <a href="#" class="btn btn-primary">My Sales Performance</a>
                                    <a href="#" class="btn btn-primary">Team Sales Performance</a>
                                </div>
                            </div>





                    </div>
                </div>
            </div>
        </div>
        <!-- Row end -->

    </div>
    <!-- Spacer ends -->

</div>
<!-- Container fluid ends -->





@stop
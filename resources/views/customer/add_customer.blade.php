
@extends('layouts.master')

@section('title', 'Customers')

@section('content')

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="blog">
                <div class="blog-header">
                    <h5 class="blog-title">Add New Customer</h5>
                    <br>
                    <br>
                    <p>Please Fill Following Fields to add a new Customer to the System </p>
                </div>

                @if(isset($msg))
                    <div class="alert alert-success alert-white rounded no-margin">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                        <div class="icon"><i class="fa fa-fire"></i></div>
                          <strong>Cheers !</strong>{{ $msg }}
                    </div>
                @endif
                <div class="panel-body" style="border: none">


                    @foreach($errors->all() as $error)

                        <div class="form-group has-feedback">
                            <div class="alert alert-danger alert-white rounded no-margin">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                                <div class="icon"><i class="fa fa-fire"></i></div>
                                <strong>Oh snap!</strong> {{ $error }}
                            </div>
                        </div>
                    @endforeach

                </div>

                <div class="blog-body">
                     {!! Form::open(array('class' => 'form-horizontal','role'=>'form')) !!}
                        <div class="form-group">
                            <label for="Customer" class="col-sm-2 control-label">Customer Name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="name" id="name" placeholder="Customer Name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="Company" class="col-sm-2 control-label">Company Name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="company_name" id="company_name" placeholder="Company Name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="Email" class="col-sm-2 control-label">Email</label>
                            <div class="col-sm-10">
                                <input type="email" class="form-control" name="email" id="email" placeholder="Email">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="address" class="col-sm-2 control-label">Address</label>
                            <div class="col-sm-10">
                                <textarea rows="4" cols="50" type="textarea" class="form-control" name="address" id="address" placeholder="Address"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Contact Number</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="contact_number" id="contact_number" placeholder="Contact Number">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label"></label>
                            <div class="col-sm-10">
                                <button type="submit" class="btn btn-default">Sign in</button>
                                <button type="reset" class="btn btn-default">Clear</button>
                            </div>
                        </div>


                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>



@stop
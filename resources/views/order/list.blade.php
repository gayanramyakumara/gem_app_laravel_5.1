@extends('layouts.master')

@section('title', 'Orders')

@section('content')



        <!-- Container fluid Starts -->
<div class="container-fluid">

    <!-- Spacer starts -->
    <div class="spacer">

        <div class="row">



            <ol class="breadcrumb">
                <li><a href="#" data-original-title="" title="">Home</a></li>
                <li><a href="#" data-original-title="" title="">Dashboard</a></li>
                <li class="active">Charts</li>
            </ol>



        </div>


        <!-- Row start -->
        <div class="row">

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="blog">
                    <div class="blog-header">
                        <h5 class="blog-title">  Order Generation</h5>
                    </div>
                    <div class="blog-body">

                        <div class="col-lg-4 col-md-4">

                            <form id="defaultForm" method="post" action="http://jesus.gallery/everest/target.php" class="form-horizontal bv-form" novalidate="novalidate">


                                <fieldset>
                                    <legend>&nbsp;</legend>

                                    <div class="form-group has-feedback">
                                        <label class="col-lg-6 control-label">Email Address</label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" name="email" data-bv-field="email"><i class="form-control-feedback" data-bv-icon-for="email" style="display: none;"></i>
                                            <small class="help-block" data-bv-validator="notEmpty" data-bv-for="email" data-bv-result="NOT_VALIDATED" style="display: none;">The email address is required and can't be empty</small><small class="help-block" data-bv-validator="emailAddress" data-bv-for="email" data-bv-result="NOT_VALIDATED" style="display: none;">The input is not a valid email address</small></div>
                                    </div>

                                    <div class="form-group has-feedback">
                                        <label class="col-lg-6 control-label">Hex Color</label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" name="color" data-bv-field="color"><i class="form-control-feedback" data-bv-icon-for="color" style="display: none;"></i>
                                            <small class="help-block" data-bv-validator="hexColor" data-bv-for="color" data-bv-result="NOT_VALIDATED" style="display: none;">The input is not a valid hex color</small></div>
                                    </div>

                                    <div class="form-group has-feedback">
                                        <label class="col-lg-6 control-label">US Zip Code</label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" name="zipCode" data-bv-field="zipCode"><i class="form-control-feedback" data-bv-icon-for="zipCode" style="display: none;"></i>
                                            <small class="help-block" data-bv-validator="zipCode" data-bv-for="zipCode" data-bv-result="NOT_VALIDATED" style="display: none;">The input is not a valid US zip code</small></div>
                                    </div>
                                </fieldset>

                                <div class="form-group">
                                    <div class="col-lg-6 col-lg-offset-6">
                                        <button type="submit" class="btn btn-success">Submit</button>
                                    </div>
                                </div>
                            </form>

                        </div>
                        <div class="col-lg-4 col-md-4">


                            <form id="defaultForm" method="post" action="http://jesus.gallery/everest/target.php" class="form-horizontal bv-form" novalidate="novalidate">


                                <fieldset>
                                    <legend>Regular expression based validators</legend>

                                    <div class="form-group has-feedback">
                                        <label class="col-lg-6 control-label">Email Address</label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" name="email" data-bv-field="email"><i class="form-control-feedback" data-bv-icon-for="email" style="display: none;"></i>
                                            <small class="help-block" data-bv-validator="notEmpty" data-bv-for="email" data-bv-result="NOT_VALIDATED" style="display: none;">The email address is required and can't be empty</small><small class="help-block" data-bv-validator="emailAddress" data-bv-for="email" data-bv-result="NOT_VALIDATED" style="display: none;">The input is not a valid email address</small></div>
                                    </div>

                                    <div class="form-group has-feedback">
                                        <label class="col-lg-6 control-label">Hex Color</label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" name="color" data-bv-field="color"><i class="form-control-feedback" data-bv-icon-for="color" style="display: none;"></i>
                                            <small class="help-block" data-bv-validator="hexColor" data-bv-for="color" data-bv-result="NOT_VALIDATED" style="display: none;">The input is not a valid hex color</small></div>
                                    </div>

                                    <div class="form-group has-feedback">
                                        <label class="col-lg-6 control-label">US Zip Code</label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" name="zipCode" data-bv-field="zipCode"><i class="form-control-feedback" data-bv-icon-for="zipCode" style="display: none;"></i>
                                            <small class="help-block" data-bv-validator="zipCode" data-bv-for="zipCode" data-bv-result="NOT_VALIDATED" style="display: none;">The input is not a valid US zip code</small></div>
                                    </div>

                                    <div class="form-group has-feedback">
                                        <label class="col-lg-6 control-label">US Zip Code</label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" name="zipCode" data-bv-field="zipCode"><i class="form-control-feedback" data-bv-icon-for="zipCode" style="display: none;"></i>
                                            <small class="help-block" data-bv-validator="zipCode" data-bv-for="zipCode" data-bv-result="NOT_VALIDATED" style="display: none;">The input is not a valid US zip code</small></div>
                                    </div>

                                </fieldset>


                            </form>

                        </div>
                        <div class="col-lg-4 col-md-4">

                            <form id="defaultForm" method="post" action="http://jesus.gallery/everest/target.php" class="form-horizontal bv-form" novalidate="novalidate">


                                <fieldset>
                                    <legend>Regular expression based validators</legend>

                                    <div class="form-group has-feedback">
                                        <label class="col-lg-6 control-label">Email Address</label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" name="email" data-bv-field="email"><i class="form-control-feedback" data-bv-icon-for="email" style="display: none;"></i>
                                            <small class="help-block" data-bv-validator="notEmpty" data-bv-for="email" data-bv-result="NOT_VALIDATED" style="display: none;">The email address is required and can't be empty</small><small class="help-block" data-bv-validator="emailAddress" data-bv-for="email" data-bv-result="NOT_VALIDATED" style="display: none;">The input is not a valid email address</small></div>
                                    </div>

                                    <div class="form-group has-feedback">
                                        <label class="col-lg-6 control-label">Hex Color</label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" name="color" data-bv-field="color"><i class="form-control-feedback" data-bv-icon-for="color" style="display: none;"></i>
                                            <small class="help-block" data-bv-validator="hexColor" data-bv-for="color" data-bv-result="NOT_VALIDATED" style="display: none;">The input is not a valid hex color</small></div>
                                    </div>

                                    <div class="form-group has-feedback">
                                        <label class="col-lg-6 control-label">US Zip Code</label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" name="zipCode" data-bv-field="zipCode"><i class="form-control-feedback" data-bv-icon-for="zipCode" style="display: none;"></i>
                                            <small class="help-block" data-bv-validator="zipCode" data-bv-for="zipCode" data-bv-result="NOT_VALIDATED" style="display: none;">The input is not a valid US zip code</small></div>
                                    </div>
                                </fieldset>

                                <div class="form-group">
                                    <div class="col-lg-6 col-lg-offset-6">
                                        <button type="submit" class="btn btn-success">Submit</button>
                                    </div>
                                </div>
                            </form>

                        </div>

                    </div>
                </div>
            </div>


        </div>
        <!-- Row end -->


        <!-- Row start -->
        <div class="row">

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="blog">
                    <div class="blog-header">
                        <h5 class="blog-title">Sublot Details  </h5>
                    </div>
                    <div class="blog-body" style="  overflow: scroll!important">
                        <table class="table no-margin">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Table heading</th>
                                <th>Table heading</th>
                                <th>Table heading</th>
                                <th>Table heading</th>
                                <th>Table heading</th>
                                <th>Table heading</th>
                                <th>#</th>
                                <th>Table heading</th>
                                <th>Table heading</th>
                                <th>Table heading</th>
                                <th>Table heading</th>
                                <th>Table heading</th>
                                <th>Table heading</th>
                                <th>#</th>
                                <th>Table heading</th>
                                <th>Table heading</th>
                                <th>Table heading</th>
                                <th>Table heading</th>
                                <th>Table heading</th>
                                <th>Table heading</th>
                                <th>#</th>
                                <th>Table heading</th>
                                <th>Table heading</th>
                                <th>Table heading</th>
                                <th>Table heading</th>
                                <th>Table heading</th>
                                <th>Table heading</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>3</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>1</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>1</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>1</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>1</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>2</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>1</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>1</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>3</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>1</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


        </div>
        <!-- Row end -->

    </div>
    <!-- Spacer ends -->

</div>
<!-- Container fluid ends -->





@stop
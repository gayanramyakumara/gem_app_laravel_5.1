<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class OrdersController extends Controller
{
    public  function __construct(){
        $this->middleware('auth');
    }


    public function index(){
        //return Auth::user()->name;
        return  view('order.list');
    }
}

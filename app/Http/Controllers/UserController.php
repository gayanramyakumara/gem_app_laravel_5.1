<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use DB;
class UserController extends Controller
{
    public  function __construct(){
        $this->middleware('auth');
    }


    public function profile($id){

        $user_details = DB::table('users')
            ->select('id','name','email','role','created_at')
            ->where('id', '=', $id)
            ->get();

         return  view('user.profile',['user_details' => $user_details]);
    }
}

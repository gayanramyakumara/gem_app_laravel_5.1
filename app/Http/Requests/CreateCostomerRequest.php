<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateCostomerRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required',
            'company_name'=>'required',
            'email'=>'required|email',
            'address'=>'required|min:10',
            'contact_number'=>'required'
        ];
    }
}

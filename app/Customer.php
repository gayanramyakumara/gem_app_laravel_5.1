<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = ['name', 'company_name','email', 'address' ,'contact_number'];


    public function setCreatedAtAttribute($date){
        $this->attributes['created_at']= Carbon::now();
    }





}
